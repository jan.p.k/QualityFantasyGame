﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Timers;

namespace TheSearch
{
    public class Boss : Enemy
    {
        private bool clipping;
        private bool isCharging;
        private MoveDir chargeDir;
        private Timer chargeTimer;
        private Bitmap fireballImg;
        private List<Fireball> balls;

        public Boss(RectangleF bounds, Bitmap image, PointF playerPos)
            : base(bounds, image, playerPos, 2, 500, 10)
        {
            fireballImg = new Bitmap("img/fire.png");

            balls = new List<Fireball>();
            clipping = false;

            chargeTimer = new Timer(5000);
            chargeTimer.Elapsed += new ElapsedEventHandler(PerformCharge);
            chargeTimer.Start();
        }

        protected override void Act(object sender, ElapsedEventArgs args)
        {
            try {
                float x = Bounds.X - PlayerPos.X, y = Bounds.Y - PlayerPos.Y;

                if (Timer.Interval != 100)
                    Timer.Interval = 100;

                if (!isCharging)
                {
                    if (!clipping)
                    {
                        if (x > 10)
                            Move(MoveDir.Right);
                        else if (x < -10)
                            Move(MoveDir.Left);
                        else
                            RemoveDir(MoveDir.Right | MoveDir.Left);

                        if (y > 10)
                            Move(MoveDir.Down);
                        else if (y < -10)
                            Move(MoveDir.Up);
                        else
                            RemoveDir(MoveDir.Up | MoveDir.Down);
                    }
                    else 
                    {
                        RemoveDir(MoveDir.Up | MoveDir.Down | MoveDir.Right | MoveDir.Left);
                        Move((MoveDir)(int)Math.Pow(2, Rand.Next(4)));
                        clipping = false;
                        Timer.Interval = 500;
                    }
                }
                else
                {
                    RemoveDir(MoveDir.Up | MoveDir.Down | MoveDir.Right | MoveDir.Left);
                    Move(chargeDir);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("BossAct");
                Console.WriteLine(e.StackTrace);
            }
        }

        private void Shoot()
        {
            try {
                int max = ((100 * Health / MaxHealth) > 20) ? 3 : 6;
                for (int i = 0; i < max; i++)
                {
                    PointF center = CalcCenter();

                    lock (fireballImg)
                    {
                        PointF point = new PointF(center.X - fireballImg.Size.Width / 2.0f, center.Y - fireballImg.Size.Height / 2.0f);

                        for (int x = 0; x < 4; x++)
                        {
                            lock (balls)
                            {
                                Fireball fb = new Fireball(new RectangleF(point, fireballImg.Size), fireballImg, 20, false);
                                fb.FireballCollided += new FireballCollideHandler(RemoveFireball);
                                fb.Move((MoveDir)(int)Math.Pow(2, x));
                                balls.Add(fb);
                            }
                        }
                    }
                    System.Threading.Thread.Sleep(330);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("BossShoot");
                Console.WriteLine(e.StackTrace);
            }
        }

        private void RemoveFireball(Fireball ball)
        {
            lock (ball)
            {
                CollisionObject.RemoveCollider(ball);
                lock (balls)
                    balls.Remove(ball);
            }
        }

        private void PerformCharge(object sender, ElapsedEventArgs args)
        {
            try {
                if (Health > 0)
                {
                    isCharging = true;
                    Speed = 8;
                    Timer.Interval = 100;

                    if (Math.Abs(PlayerPos.X - Bounds.X) < 50) { }
                    else if (PlayerPos.X < Bounds.X)
                        chargeDir |= MoveDir.Left;
                    else
                        chargeDir |= MoveDir.Right;

                    if (Math.Abs(PlayerPos.Y - Bounds.Y) < 50) { }
                    else if (PlayerPos.Y <= Bounds.Y)
                        chargeDir |= MoveDir.Up;
                    else
                        chargeDir |= MoveDir.Down;

                    System.Threading.Thread shootThread = new System.Threading.Thread(Shoot);
                    shootThread.Start();
                    while (!shootThread.IsAlive);
                    System.Threading.Thread.Sleep(1500);

                    isCharging = false;
                    Speed = 2;
                    chargeDir = 0;
                }
                else
                    chargeTimer.Stop();
            }
            catch (Exception e)
            {
                Console.WriteLine("BossCharge");
                Console.WriteLine(e.StackTrace);
            }
        }

        protected override void Collide(CollisionObject collider)
        {
            base.Collide(collider);

            if (collider.GetType().Equals(typeof(Wall)))
                clipping = true;
        }

        public override void Draw(Graphics grph)
        {
            float tmp = Rotation;
            Rotation = 0;
            base.Draw(grph);
            Rotation = tmp;

            lock (balls)
            {
                for (int i = 0; i < balls.Count; i++)
                {
                    if (balls[i] != null)
                    {
                        lock (balls[i])
                            balls[i].Draw(grph);
                    }
                }
            }
        }
    }
}
