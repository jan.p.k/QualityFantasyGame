﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace TheSearch
{
    public struct Vector
    {
        public float X { get; set; }
        public float Y { get; set; }

        public Vector(float x, float y) : this()
        {
            this.X = x;
            this.Y = y;
        }

        public Vector(PointF point) :
            this(point.X, point.Y) {}

        public static PointF operator + (PointF point, Vector vector)
        {
            return new PointF(point.X + vector.X, point.Y + vector.Y);
        }

        public static PointF operator - (PointF point, Vector vector)
        {
            return new PointF(point.X - vector.X, point.Y - vector.Y);
        }

        public static Vector operator / (Vector vector, float dividend)
        {
            return new Vector(vector.X / dividend, vector.Y / dividend);
        }

         public static Vector operator * (Vector vector, float factor)
        {
            return new Vector(vector.X * factor, vector.Y * factor);
        }

        public override string ToString()
        {
            return "Vector { X: " + X.ToString() + ", Y: " + Y.ToString() + " }";
        }
    }
}
