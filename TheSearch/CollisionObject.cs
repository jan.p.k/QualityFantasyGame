﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace TheSearch
{
    public abstract class CollisionObject : GameObject
    {
        private static List<CollisionObject> colliders = new List<CollisionObject>();

        protected static CollisionObject[] Colliders 
        { 
            get 
            { 
                lock (colliders) 
                    return colliders.ToArray(); 
            } 
        }

        public static void RemoveCollider(CollisionObject collider)
        {
            lock (colliders)
            {
                colliders.Remove(collider);
            }
        }

        public static void ClearColliders()
        {
            lock (colliders)
                colliders = (from collider in colliders where collider.GetType().Equals(typeof(Player)) select collider).ToList();
        }

        protected CollisionObject(RectangleF bounds, Bitmap image)
            : base(bounds, image) 
        {
            lock (colliders)
                colliders.Add(this);
        }

        protected abstract void Collide(CollisionObject collider);

        public override void Draw(Graphics grph)
        {
            base.Draw(grph);
            for (int i = 0; i < CollisionObject.colliders.Count; i++)
            {
                if (CollisionObject.colliders.Contains(this))
                {
                    CollisionObject collider = CollisionObject.colliders[i];
                    if (this != collider && Bounds.IntersectsWith(collider.Bounds))
                        Collide(collider);
                }
            }
        }
    }
}
