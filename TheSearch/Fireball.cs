﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace TheSearch
{
    public delegate void FireballCollideHandler(Fireball ball);

    public class Fireball : Projectile
    { 
        public event FireballCollideHandler FireballCollided;

        public Fireball(RectangleF bounds, Bitmap image, int damage, bool friendly)
            : base(bounds, image, damage, 10, friendly)
        {
            
        }


        protected override void Collide(CollisionObject collider)
        {
            if (collider.GetType().IsSubclassOf(typeof(Actor)))
            {
                if ((Friendly && collider.GetType().IsSubclassOf(typeof(Enemy))) || 
                    (!Friendly && collider.GetType().Equals(typeof(Player))))
                        ((Actor)collider).Hit(Damage);
            }
            else if (collider.GetType().Equals(typeof(Wall)))
            {
                if (FireballCollided != null)
                    FireballCollided(this);
            }
        }
    }
}
