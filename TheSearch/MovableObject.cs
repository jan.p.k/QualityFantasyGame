﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Timers;

namespace TheSearch
{
    public delegate void MoveHandler(PointF position);

    public abstract class MovableObject : CollisionObject
    {
        private int speed;
        private Vector vector;
        private Timer timer;
        private Wall wallCollider;

        public event MoveHandler Moved;

        protected int Speed 
        {
            get { return speed; }
            set
            { 
                speed = value;
                if (vector.X > 0) vector.X = speed;
                else if (vector.X < 0) vector.X = -speed;

                if (vector.Y > 0) vector.Y = speed;
                else if (vector.Y < 0) vector.Y = -speed;
            }
        }

        protected MovableObject(RectangleF bounds, Bitmap image, int speed)
            : base (bounds, image) 
        {
            this.speed = speed;
            vector = new Vector(0, 0);

            timer = new Timer(10);
            timer.Elapsed += new ElapsedEventHandler(Step);
            timer.Start();
        }

        public virtual void Move(MoveDir dir) 
        {
            if ((dir & MoveDir.Up) == MoveDir.Up)
                vector = new Vector(vector.X, -speed); 
            else if ((dir & MoveDir.Down) == MoveDir.Down)
                vector = new Vector(vector.X, speed);

            if ((dir & MoveDir.Right) == MoveDir.Right)
                vector = new Vector(speed, vector.Y);
            else if ((dir & MoveDir.Left) == MoveDir.Left)
                vector = new Vector(-speed, vector.Y);
        }

        public void RemoveDir(MoveDir dir)
        {
            if ((dir & MoveDir.Up) == MoveDir.Up || (dir & MoveDir.Down) == MoveDir.Down)
                vector = new Vector(vector.X, 0);
            if ((dir & MoveDir.Right) == MoveDir.Right || (dir & MoveDir.Left) == MoveDir.Left)
                vector = new Vector(0, vector.Y);
            
        }

        protected virtual void Step(object sender, ElapsedEventArgs args)
        {
            try {
                bool move = true;
                Rotation = CalcAngle();
                lock (CollisionObject.Colliders)
                {
                    foreach (Wall wall in (from collider in CollisionObject.Colliders where collider.GetType().Equals(typeof(Wall)) select collider).ToArray())
                    {
                        if (wall.Bounds.IntersectsWith(new RectangleF(Bounds.Location + vector, Bounds.Size)))
                        {
                            move = false;
                            if (wallCollider != null)
                            {
                                lock (wallCollider)
                                    wallCollider = wall;
                            }
                            else
                                wallCollider = wall;
                        }
                    }
                }

                if (move || this.GetType().Equals(typeof(Ghost))) // Ghosts can go though walls
                {
                    bounds.Location += vector;
                    if (Moved != null && (vector.X != 0 || vector.Y != 0))
                        Moved(Bounds.Location);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("MoveThread");
                Console.WriteLine(e.StackTrace);
            }
        }

        private float CalcAngle()
        {
            if ((vector.X == 0 && vector.Y < 0))
                return 0;
            else if (vector.X > 0 && vector.Y < 0)
                return 45;
            else if (vector.X > 0 && vector.Y == 0)
                return 90;
            else if (vector.X > 0 && vector.Y > 0)
                return 135;
            else if (vector.X == 0 && vector.Y > 0)
                return 180;
            else if (vector.X < 0 && vector.Y > 0)
                return 225;
            else if (vector.X < 0 && vector.Y == 0)
                return 270;
            else if (vector.X < 0 && vector.Y < 0)
                return 315;
            else
                return Rotation;
        }

        public override void Draw(Graphics grph)
        {
            base.Draw(grph);
            if (wallCollider != null)
            {
                lock (wallCollider)
                {
                    Collide(wallCollider);
                    wallCollider = null;
                }
            }
        }
    }

    [Flags]
    public enum MoveDir
    {
        Up = 1, 
        Down = 2, 
        Right = 4, 
        Left = 8
    }
}
