﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.IO;

namespace TheSearch
{
    public partial class MainWin : Form
    {
        private bool showWinningScreen, showLosingScreen;
        private int roomCount;
        private DateTime heavyLoader;
        private Room room;
        private Player player;
        private Class selectedClass;
        private Timer drawTimer;

        private enum Class { Knight, Archer, Mage }

        public MainWin()
        {
            InitializeComponent();
        }

        public void ClassButtonClicked(object sender, EventArgs args)
        {
            okButton.Enabled = true;
            Button b = (Button)sender;

            knightBox.Visible = false;
            archerBox.Visible = false;
            paladinBox.Visible = false;

            if (b.Equals(knightButton))
            {
                knightBox.Visible = true;
                selectedClass = Class.Knight;
            }
            else if (b.Equals(archerButton))
            {
                archerBox.Visible = true;
                selectedClass = Class.Archer;
            }
            else
            {
                paladinBox.Visible = true;
                selectedClass = Class.Mage;
            }
        }

        private void OkButtonClicked(object sender, EventArgs e)
        {
            this.Controls.Clear();
            StartGame();
        }

        private void OnPaint(object sender, PaintEventArgs args) 
        {
            try {
                room.Draw(args.Graphics);
                player.Draw(args.Graphics);
                if (showWinningScreen)
                    args.Graphics.DrawImage(new Bitmap("img/won.png"), ClientRectangle);
                else if (showLosingScreen)
                    args.Graphics.DrawImage(new Bitmap("img/lost.png"), ClientRectangle);
            }
            catch (Exception e)
            {
                Console.WriteLine("OnPaint");
                Console.WriteLine(e.StackTrace);
            }
        }

        private void OnMouseDown(object sender, MouseEventArgs args)
        {
            if (args.Button == MouseButtons.Left)
            {
                if (player.Stamina > Player.HEAVY_CONSUMTION)
                {
                    heavyLoader = DateTime.Now;
                    player.StartCharging();
                }
            }
        }

        private void OnMouseUp(object sender, MouseEventArgs args)
        {
            if (args.Button == MouseButtons.Left)
            {
                int span = (int)(DateTime.Now - heavyLoader).TotalMilliseconds;
                if (span > 750)
                    player.HeavyAttack(room.Enemies);
                else
                    player.FastAttack(room.Enemies);

                player.EndCharging();
            }
            else if (args.Button == MouseButtons.Right)
                player.Cast();
        }

        private void OnKeyDown(object sender, KeyEventArgs args)
        {
            switch (args.KeyCode)
            {
                case Keys.Up:
                case Keys.W:
                    player.Move(MoveDir.Up); 
                    break;
                case Keys.Down:
                case Keys.S:
                     player.Move(MoveDir.Down); 
                    break;
                case Keys.Right:
                case Keys.D:
                     player.Move(MoveDir.Right); 
                    break;
                case Keys.Left:
                case Keys.A:
                     player.Move(MoveDir.Left); 
                    break;
                case Keys.ShiftKey:
                    player.StartSprint();
                    break;
                case Keys.D1:
                    player.Drink(PotionType.Health);
                    break;
                case Keys.D2:
                    player.Drink(PotionType.Stamina);
                    break;
                case Keys.D3:
                    player.Drink(PotionType.Mana);
                    break;
                case Keys.Return:
                case Keys.Space:
                    ChangeRoom();
                    break;
            }
        }

        private void OnKeyUp(object sender, KeyEventArgs args)
        {
            switch (args.KeyCode)
            {
                case Keys.Up:
                case Keys.Down:
                case Keys.W:
                case Keys.S:
                    player.RemoveDir(MoveDir.Up | MoveDir.Down);
                    break;
                case Keys.Right:
                case Keys.Left:
                case Keys.D:
                case Keys.A:
                    player.RemoveDir(MoveDir.Right | MoveDir.Left);
                    break;
                case Keys.ShiftKey:
                    player.EndSprint();
                    break;
            }
        }

        private void OnPlayerMove(PointF position)
        {
            foreach (Enemy enemy in room.Enemies)
                enemy.UpdatePlayerPos(position);

            heavyLoader = DateTime.Now;
            player.EndCharging();
        }

        private void OnEnemyDeath(Actor actor)
        {
            Random rand = new Random();
            int probability;

            if (actor.GetType().Equals(typeof(Bat)))
                probability = 15;
            else if (actor.GetType().Equals(typeof(Ghost)))
                probability = 30;
            else
                probability = 40;

            if (rand.Next(100) < probability)
                player.AddPotion((PotionType)rand.Next(3));

            if (actor.GetType().Equals(typeof(Boss)))
            {
                KeyDown -= OnKeyDown;
                MouseDown -= OnMouseDown;
                drawTimer.Stop();
                showWinningScreen = true;
                Invalidate();
            }
        }

        private void OnPlayerDeath(Actor actor)
        {
            KeyDown -= OnKeyDown;
            MouseDown -= OnMouseDown;
            drawTimer.Stop();
            showLosingScreen = true;
            Invalidate();
        }

        private void ChangeRoom()
        {
            if (player.Bounds.IntersectsWith(new RectangleF(1000, 256, 50, 90)) && roomCount <= 10)
            {
                player.SetPosition(new PointF(160, 275));
                CollisionObject.ClearColliders();
                if (player.Weapon.GetType().Equals(typeof(Bow)))
                    ((Bow)player.Weapon).Reset();

                Room room;
                if (roomCount < 10)
                {
                    string[] files = Directory.GetFiles("res/", "room*.room");
                    room = LoadRoom(files[new Random().Next(files.Length)]);
                }
                else
                    room = LoadRoom("res/bossRoom.room");

                this.room = room;
                roomCount++;
            }
        }

        private Room LoadRoom(string roomPath)
        {
            Room room;
            List<Enemy> enemies = new List<Enemy>();
            List<Wall> walls = new List<Wall>();
            Bitmap bg, enemyImg, wallImg;
            int type;
            Point pos;
            Rectangle wallBounds;
            using (StreamReader reader = new StreamReader(new FileStream(roomPath, FileMode.Open)))
            {
                bg = new Bitmap("img/rooms/" + reader.ReadLine());
                while (reader.Peek() != '\u001D')
                {
                    System.Threading.Thread.Sleep(20);
                    enemyImg = new Bitmap("img/" + reader.ReadLine());
                    type = Convert.ToInt32(reader.ReadLine());
                    pos = new Point(Convert.ToInt32(reader.ReadLine()), Convert.ToInt32(reader.ReadLine()));
                    
                    Enemy enemy;
                    switch (type)
                    {
                        case 0:
                            enemy = new Bat(new RectangleF(pos, enemyImg.Size), enemyImg);
                            break;
                        case 1:
                            enemy = new Ghost(new RectangleF(pos, enemyImg.Size), enemyImg, player.Bounds.Location);
                            break;
                        case 2:
                            enemy = new Ghoul(new RectangleF(pos, enemyImg.Size), enemyImg, player.Bounds.Location);
                            break;
                        case 3:
                            enemy = new Skeleton(new RectangleF(pos, enemyImg.Size), enemyImg, player.Bounds.Location);
                            break;
                        default:
                            enemy = new Boss(new RectangleF(pos, enemyImg.Size), enemyImg, player.Bounds.Location);
                            break;
                    }
                    enemy.Died += new DeathHandler(OnEnemyDeath);
                    enemies.Add(enemy);
                }
                reader.ReadLine();
                while (reader.Peek() != '\u001D')
                {
                    wallImg = new Bitmap("img/rooms/" + reader.ReadLine());
                    wallBounds = new Rectangle(Convert.ToInt32(reader.ReadLine()), Convert.ToInt32(reader.ReadLine()), Convert.ToInt32(reader.ReadLine()), Convert.ToInt32(reader.ReadLine()));
                    walls.Add(new Wall(wallBounds, wallImg));
                }
                reader.ReadLine();
                room = new Room(new RectangleF(new PointF(0, 0), bg.Size), bg, enemies.ToArray(), walls.ToArray());
            }
            return room;
        }

        private void StartGame()
        {
            roomCount = 0;
            showWinningScreen = false;
            showLosingScreen = false;
            Bitmap mapBg = new Bitmap("img/rooms/map0.png");
            ClientSize = mapBg.Size;
            RectangleF size = this.ClientRectangle;

            if (selectedClass == Class.Knight)
            {
                Bitmap weaponImg = new Bitmap("img/sword.png");
                Bitmap weaponAnim = new Bitmap("img/swordAnim.gif");
                Bitmap weaponAnimHeavy = new Bitmap("img/swordAnimHeavy.gif");
                Melee weapon = new Melee(new RectangleF(new PointF(150, 642), weaponImg.Size), weaponImg, weaponAnim, weaponAnimHeavy, 25, 30);
                Bitmap[] playerImgs = { 
                    new Bitmap("img/player/knightUp.png"), 
                    new Bitmap("img/player/knightDown.png"),
                    new Bitmap("img/player/knightRight.png"),
                    new Bitmap("img/player/knightLeft.png"),
                };
                player = new Player(new RectangleF(new PointF(200, 200), playerImgs[0].Size), playerImgs, weapon, 4, 150, 75, 75, SpellType.Invincible);
            }
            else if (selectedClass == Class.Archer)
            {
                Bitmap weaponImg = new Bitmap("img/bow.png");
                Bitmap weaponAnim = new Bitmap("img/bowAnim.png");
                Bitmap weaponAnimHeavy = new Bitmap("img/bowAnimHeavy.png");
                Bitmap boltFast = new Bitmap("img/bolt.png");
                Bitmap boltHeavy = new Bitmap("img/boltHeavy.png");
                Bow weapon = new Bow(new RectangleF(new PointF(150, 642), weaponImg.Size), weaponImg, weaponAnim, weaponAnimHeavy, boltFast, boltHeavy, 20, 3, true);
                Bitmap[] playerImgs = { 
                    new Bitmap("img/player/archerUp.png"), 
                    new Bitmap("img/player/archerDown.png"),
                    new Bitmap("img/player/archerRight.png"),
                    new Bitmap("img/player/archerLeft.png"),
                };
                player = new Player(new RectangleF(new PointF(200, 200), playerImgs[0].Size), playerImgs, weapon, 6, 75, 150, 75, SpellType.Damage);
            }
            else
            {
                Bitmap weaponImg = new Bitmap("img/staff.png");
                Bitmap weaponAnim = new Bitmap("img/staffAnim.gif");
                Bitmap weaponAnimHeavy = new Bitmap("img/staffAnimHeavy.gif");
                Melee weapon = new Melee(new RectangleF(new PointF(150, 642), weaponImg.Size), weaponImg, weaponAnim, weaponAnimHeavy, 20, 70);
                Bitmap[] playerImgs = { 
                    new Bitmap("img/player/paladinUp.png"), 
                    new Bitmap("img/player/paladinDown.png"),
                    new Bitmap("img/player/paladinRight.png"),
                    new Bitmap("img/player/paladinLeft.png"),
                };
                player = new Player(new RectangleF(new PointF(200, 200), playerImgs[0].Size), playerImgs, weapon, 5, 75, 75, 150, SpellType.Fireball);
            }
            
            player.Died += new DeathHandler(OnPlayerDeath);
            player.Moved += new MoveHandler(OnPlayerMove);
            player.AddPotion(PotionType.Health);
            player.AddPotion(PotionType.Stamina);
            player.AddPotion(PotionType.Mana);

            room = LoadRoom("res/startRoom.room");

            MouseDown += new MouseEventHandler(OnMouseDown);
            MouseUp += new MouseEventHandler(OnMouseUp);
            KeyDown += new KeyEventHandler(OnKeyDown);
            KeyUp += new KeyEventHandler(OnKeyUp);
            Paint += new PaintEventHandler(OnPaint);

            drawTimer = new Timer();
            drawTimer.Interval = 1;
            drawTimer.Tick += (s, e) => this.Invalidate();
            drawTimer.Start();
        }
    }
}
