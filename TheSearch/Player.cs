﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Timers;

namespace TheSearch
{
    public class Player : Actor
    {
        public const int HEAVY_CONSUMTION = 30;

        private bool isSpellProtected;
        private bool isSprinting;
        private bool isCharging;
        private int defaultSpeed;
        private int stamina, maxStamina;
        private int mana, maxMana;
        private DateTime chargingStart;
        private Weapon weapon;
        private Bitmap[] images;
        private Timer healthTimer, staminaTimer, manaTimer;
        private SolidBrush staminaBrush, manaBrush, chargeBrush;
        private Tuple<PotionType, DateTime, bool> blinkStart;
        private KeyValuePair<Potion, int>[] potions;
        private List<Fireball> balls;
        private Bitmap fireballImg;

        public delegate void Spell();
        private Spell cast;

        public int Stamina 
        {
            get { return stamina; }
            set { stamina = value; }
        }

        public int Mana
        {
            get { return mana; }
            set { mana = value; }
        }

        public Weapon Weapon { get { return weapon; } }

        public Spell Cast { get { return cast; } }


        public Player(RectangleF bounds, Bitmap[] images, Weapon weapon, int speed, int health, int stamina, int mana, SpellType spellType)
            : base(bounds, images[0], speed, health)
        {
            this.images = images;
            this.weapon = weapon;
            defaultSpeed = speed;

            this.stamina = stamina;
            maxStamina = stamina;

            this.mana = mana;
            maxMana = mana;

            balls = new List<Fireball>();
            fireballImg = new Bitmap("img/fire.png");

            switch (spellType)
            {
                case SpellType.Fireball:
                    cast = SpellFireball;
                    break;
                case SpellType.Invincible:
                    cast = SpellInvincible;
                    break;
                default:
                    cast = SpellDamage;
                    break;
            }

            Bitmap[] potionsImgs = new Bitmap[] {
                new Bitmap("img/potion_red.png"),
                new Bitmap("img/potion_green.png"),
                new Bitmap("img/potion_blue.png")
            };
            potions = new KeyValuePair<Potion, int>[] {
                new KeyValuePair<Potion, int>(new Potion(new RectangleF(new PointF(300, 642), potionsImgs[0].Size), potionsImgs[0], new Bitmap("img/potionAnim.gif"), 50, 0, 0), 0),
                new KeyValuePair<Potion, int>(new Potion(new RectangleF(new PointF(450, 642), potionsImgs[1].Size), potionsImgs[1], new Bitmap("img/potionAnim.gif"), 0, 50, 0), 0),
                new KeyValuePair<Potion, int>(new Potion(new RectangleF(new PointF(600, 642), potionsImgs[2].Size), potionsImgs[2], new Bitmap("img/potionAnim.gif"), 0, 0, 50), 0)
            };

            staminaBrush = new SolidBrush(Color.Green);
            manaBrush = new SolidBrush(Color.Blue);
            chargeBrush = new SolidBrush(Color.Gold);

            isSprinting = false;
            healthTimer = new Timer(1500);
            healthTimer.Elapsed += new ElapsedEventHandler(RegenerateHealth);
            staminaTimer = new Timer(750);
            staminaTimer.Elapsed += new ElapsedEventHandler(RegenerateStamina);
            manaTimer = new Timer(750);
            manaTimer.Elapsed += new ElapsedEventHandler(RegenerateMana);
            healthTimer.Start();
            staminaTimer.Start();
            manaTimer.Start();
        }        

        public void FastAttack(Enemy[] enemies)
        {
            weapon.FastAttack((int)Rotation, Bounds, enemies);
        }

        public void HeavyAttack(Enemy[] enemies)
        {
            if (stamina >= HEAVY_CONSUMTION)
            {
                stamina -= HEAVY_CONSUMTION;
                weapon.HeavyAttack((int)Rotation, Bounds, enemies);
            }
        }

        protected override void Collide(CollisionObject collider)
        {
            // Not Handled
        }

        public override void Move(MoveDir dir)
        {
            base.Move(dir);
            try 
            {
                Image = images[(int)Math.Log((int)dir, 2)];
            }
            catch (IndexOutOfRangeException) {}
        }

        public void StartSprint()
        {
            if (!isSprinting && stamina > 0)
            {
                Speed = (int)(Speed * 1.5);
                isSprinting = true;
            }
        }

        public void EndSprint()
        {
            Speed = defaultSpeed;
            isSprinting = false;
        }

        protected override void Step(object sender, ElapsedEventArgs args)
        {
            base.Step(sender, args);
            weapon.ActorCenter = CalcCenter();
            foreach (KeyValuePair<Potion, int> potion in potions)
                potion.Key.ActorCenter = CalcCenter();

            if (isSprinting)
            {
                stamina -= 1;
                if (stamina <= 0)
                    EndSprint();
            }
        }

        public override void Hit(int damage)
        {
            base.Hit(damage);
            if (!IsProtected && isSpellProtected)
                Health += damage;
        }

        public void SetPosition(PointF pos)
        {
            bounds.Location = pos;
        }

        private void RegenerateHealth(object sender, ElapsedEventArgs args)
        {
            try {
                if (Health < MaxHealth)
                {
                    if (Health + 1 <= MaxHealth)
                        Health++;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Regenerate");
                Console.WriteLine(e.StackTrace);
            }
        }

        private void RegenerateStamina(object sender, ElapsedEventArgs args)
        {
            try {
                if (stamina < maxStamina)
                {
                    if (stamina + 1 <= maxStamina)
                        stamina++;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("RegenerateStamina");
                Console.WriteLine(e.StackTrace);
            }
        }

        private void RegenerateMana(object sender, ElapsedEventArgs args)
        {
            try {
                if (mana < maxMana)
                {
                    if (mana + 1 <= maxMana)
                        mana++;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("RegenerateMana");
                Console.WriteLine(e.StackTrace);
            }
        }

        public void StartCharging()
        {
            if (stamina > HEAVY_CONSUMTION)
            {
                isCharging = true;
                chargingStart = DateTime.Now;
            }
        }

        public void EndCharging()
        {
            isCharging = false;
        }

        public void AddPotion(PotionType type)
        {
            int index = (int)type;
            potions[index] = new KeyValuePair<Potion, int>(potions[index].Key, potions[index].Value + 1);

            blinkStart = new Tuple<PotionType, DateTime, bool>(type, DateTime.Now, true);
        }

        public void Drink(PotionType type)
        {
            int index = (int)type;
            if (potions[index].Value > 0)
            {
                Potion potion = potions[index].Key;
                potion.Drink(this);

                if (Health > MaxHealth)
                    Health = MaxHealth;
                if (stamina > maxStamina)
                    stamina = maxStamina;
                if (mana > maxMana)
                    mana = maxMana;

                potions[index] = new KeyValuePair<Potion, int>(potions[index].Key, potions[index].Value - 1);

                blinkStart = new Tuple<PotionType, DateTime, bool>(type, DateTime.Now, false);
            }
        }

        private void SpellFireball()
        {
            if (mana >= 50)
            {
                PointF center = CalcCenter();
                PointF point = new PointF(center.X - fireballImg.Size.Width / 2.0f, center.Y - fireballImg.Size.Height / 2.0f);

                balls.Add(new Fireball(new RectangleF(point, fireballImg.Size), fireballImg, 50, true));

                if (Rotation < 90 || Rotation > 270)
                    balls.Last().Move(MoveDir.Up);
                else if (Rotation > 90 && Rotation < 270)
                    balls.Last().Move(MoveDir.Down);

                if (Rotation > 0 && Rotation < 180)
                    balls.Last().Move(MoveDir.Right);
                else if (Rotation < 360 && Rotation > 180)
                    balls.Last().Move(MoveDir.Left);

                balls.Last().FireballCollided += (ball) => {
                    CollisionObject.RemoveCollider(ball);
                    balls.Remove(ball);
                };

                mana -= 50;
            }
        }
        private void SpellDamage()
        {
            if (mana >= 75)
            {
                System.Threading.Thread damageThread = new System.Threading.Thread(StartDamageCountDown);
                damageThread.Start();
                while (!damageThread.IsAlive);
                mana -= 75;
            }
        }
        private void SpellInvincible()
        {
            if (mana >= 75)
            {
                System.Threading.Thread protectionThread = new System.Threading.Thread(StartSpellProtection);
                protectionThread.Start();
                while (!protectionThread.IsAlive);
                mana -= 75;
            }
        }

        private void StartDamageCountDown()
        {
            try {
                weapon.Damage *= 2;
                System.Threading.Thread.Sleep(10000);
                weapon.Damage /= 2;
            }
            catch (Exception e)
            {
                Console.WriteLine("DamageSpell");
                Console.WriteLine(e.StackTrace);
            }
        }

        private void StartSpellProtection() 
        {
            try {
                isSpellProtected = true;
                System.Threading.Thread.Sleep(5000);
                isSpellProtected = false;
            }
            catch (Exception e)
            {
                Console.WriteLine("ProtectSpell");
                Console.WriteLine(e.StackTrace);
            }
        }

        public override void Draw(Graphics grph)
        {
            float tmp = Rotation;
            Rotation = 0;
            if (tmp >= 180)
            {
                base.Draw(grph);
                weapon.Draw(grph);
            }
            else 
            {
                weapon.Draw(grph);
                base.Draw(grph);
            }
            grph.DrawRectangle(BorderPen, weapon.Bounds.X, weapon.Bounds.Y, weapon.Bounds.Width, weapon.Bounds.Height);

            Rotation = tmp;

            for (int i = 0; i < balls.Count; i++)
                balls[i].Draw(grph);

            for (int i = 0; i < potions.Length; i++)
            {
                if (((PotionType)i) == blinkStart.Item1 && (DateTime.Now - blinkStart.Item2).TotalMilliseconds < 1000)
                    grph.FillRectangle(new SolidBrush((blinkStart.Item3) ? Color.LimeGreen : Color.Red), potions[i].Key.Bounds);
                potions[i].Key.Draw(grph);
                grph.DrawString(potions[i].Value.ToString(), new Font(FontFamily.GenericSansSerif, 20), new SolidBrush(Color.Black), potions[i].Key.Bounds.Location);
                grph.DrawRectangle(BorderPen, potions[i].Key.Bounds.X, potions[i].Key.Bounds.Y, potions[i].Key.Bounds.Width, potions[i].Key.Bounds.Height);
            }

            grph.TranslateTransform(Bounds.Location.X, Bounds.Location.Y);
            Point staminaPoint = new Point(0, -25);
            Point manaPoint = new Point(0, -15);
            int height = 10;
            grph.FillRectangle(staminaBrush, new RectangleF(staminaPoint, new SizeF(Bounds.Width * (float)stamina/(float)maxStamina, height)));
            grph.DrawRectangle(BorderPen, new Rectangle(staminaPoint, new Size((int)Bounds.Width, height)));
            grph.FillRectangle(manaBrush, new RectangleF(manaPoint, new SizeF(Bounds.Width * (float)mana/(float)maxMana, height)));
            grph.DrawRectangle(BorderPen, new Rectangle(manaPoint, new Size((int)Bounds.Width, height)));

            if (isCharging && (DateTime.Now - chargingStart).TotalMilliseconds > 200)
            {
                Point chargePoint = new Point(0, (int)Bounds.Height + 5);
                float progress = (float)(DateTime.Now - chargingStart).TotalMilliseconds / 750;
                if (progress > 1)
                    progress = 1.0f;
                grph.FillRectangle(chargeBrush, new RectangleF(chargePoint, new SizeF(Bounds.Width * progress, height)));
                grph.DrawRectangle(BorderPen, new Rectangle(chargePoint, new Size((int)Bounds.Width, height)));
            }

            grph.ResetTransform();
        }
    }

    public enum SpellType 
    {
        Fireball, Damage, Invincible
    }
}
